# PlusOne

A Laravel_Angular7_ngrx_nx educational purposes project

## Getting Started

This is an that has been built based on a few requirements:

HOMEPAGE;
LOGIN;
REGISTER;
PROFILE;
PROFILE SETTINGS;
LIST;
DETAILS;
CONTACT;
SOCIAL LOGIN VIA GOOGLE API;

### Prerequisites

MySQL ubuntu:

`sudo apt-get install -y mysql-server`

NPM ubuntu:

`sudo apt-get install npm`

Angular-cli ubuntu:

`npm install -g @angular/cli`

### Installing

To run this project:

LARAVEL:

"Backend files must be located on a apache2 environment"

`cd backend/` 

`composer install`  --- Install all dependencies

`cp .env.testing .env`  --- Copy the example env file and make the required configuration changes in the .env file

`php artisan key:generate`  --- Generate a new application key

`php artisan jwt:generate`  --- Generate a new JWT authentication secret key

`php artisan migrate`   --- Run the database migrations

`php artisan serve` --- Start the local development server

ANGULAR:

`cd frontend/`

`npm install`   --- Install all dependencies

`ng serve`  ---  Start angular

`ng test`   ---  Run angular tests

## Built With

* [Angular](https://angularjs.io/) - The frontend framework used
* [Laravel](https://laravel.com/) - The backend framework used
* [NPM](https://www.npmjs.com/) - Dependency Management
* [Babel](https://babeljs.io/) - JavaScript compiler
* [NGRX](https://github.com/ngrx) - NGRX
* [NX](https://nrwl.io/nx) - Nrwl Extensions for Angular


## Authors

* **Sergi Chàfer** 

## Future Improvements

* **Laravel GraphQL using Apollo**
* **Clean code, never is clean enough**

## License 

This project was based on the useful goThinkster frontends/backends repos.
* [GoThinkster Realworld App Repo](https://github.com/gothinkster/realworld) - Realworld App

All of the codebases are MIT licensed unless otherwise specified.