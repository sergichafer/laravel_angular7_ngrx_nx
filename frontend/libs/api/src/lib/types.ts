export interface Profile {
  username: string;
  bio: string;
  image: string;
  following: boolean;
  loading: boolean;
}

export interface IncidenceData {
  slug: string;
  title: string;
  description: string;
  image: string;
  body: string;
  tagList: string[];
  createdAt: string;
  updatedAt: string;
}

export interface ContactData {
  title: string;
  mail: string;
  body: string;
}

export interface User {
  email: string;
  token: string;
  username: string;
  bio: string;
  image: string;
}
