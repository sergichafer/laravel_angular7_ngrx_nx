import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import {
  LoadIncidence,
} from './incidence.actions';
import { IncidenceState } from './incidence.reducer';
import { incidenceQuery } from './incidence.selectors';

@Injectable()
export class IncidenceFacade {
  incidence$ = this.store.select(incidenceQuery.getIncidenceData);
  incidenceLoaded$ = this.store.select(incidenceQuery.getIncidenceLoaded);

  constructor(private store: Store<IncidenceState>) {}

  loadIncidence(slug: string) {
    this.store.dispatch(new LoadIncidence(slug));
  }
}
