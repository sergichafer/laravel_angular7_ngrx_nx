import { ApiService } from '@plusOne/api';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { StoreModule } from '@ngrx/store';
import { hot } from '@nrwl/nx/testing';

import { IncidenceService } from '../incidence.service';
import { IncidenceEffects } from './incidence.effects';
import { ActionsService } from '@plusOne/shared';
import { NgrxFormsFacade } from '@plusOne/ngrx-forms';

describe('IncidenceEffects', () => {
	let actions;
	let effects: IncidenceEffects;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [StoreModule.forRoot({}), HttpClientTestingModule],
			providers: [IncidenceEffects, provideMockActions(() => actions), IncidenceService, ApiService, ActionsService, NgrxFormsFacade]
		});

		effects = TestBed.get(IncidenceEffects);
	});

	describe('someEffect', () => {
		it('should work', async () => {
			actions = hot('-a-|', { a: { type: 'LOAD_DATA' } });
			expect(true).toBeTruthy();
		});
	});
});
