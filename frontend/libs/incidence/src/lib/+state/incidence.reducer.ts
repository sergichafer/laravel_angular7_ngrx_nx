import { IncidenceAction, IncidenceActionTypes } from './incidence.actions';
import { IncidenceData } from '@plusOne/api';

export interface Incidence {
  data: IncidenceData;
  loading: boolean;
  loaded: boolean;
}

export interface IncidenceState {
  readonly incidence: Incidence;
}

export const incidenceInitialState: Incidence = {
  data: {
    slug: '',
    title: '',
    description: '',
    image: '',
    body: '',
    tagList: [],
    createdAt: '',
    updatedAt: ''
  },
  loaded: false,
  loading: false
};

export function incidenceReducer(state: Incidence = incidenceInitialState, action: IncidenceAction): Incidence {
  switch (action.type) {
    case IncidenceActionTypes.LOAD_INCIDENCE_SUCCESS: {
      return { ...state, data: action.payload, loaded: true, loading: false };
    }
    case IncidenceActionTypes.LOAD_INCIDENCE_FAIL: {
      return { ...state, data: incidenceInitialState.data, loaded: false, loading: false };
    }
    default: {
      return state;
    }
  }
}
