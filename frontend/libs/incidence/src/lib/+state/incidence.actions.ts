import { Action } from '@ngrx/store';
import { IncidenceData } from '@plusOne/api';

export enum IncidenceActionTypes {
  LOAD_INCIDENCE = '[incidence] LOAD_INCIDENCE',
  LOAD_INCIDENCE_SUCCESS = '[incidence] LOAD_INCIDENCE_SUCCESS',
  LOAD_INCIDENCE_FAIL = '[incidence] LOAD_INCIDENCE_FAIL'
}

export class LoadIncidence implements Action {
  readonly type = IncidenceActionTypes.LOAD_INCIDENCE;
  constructor(public payload: string) {}
}

export class LoadIncidenceSuccess implements Action {
  readonly type = IncidenceActionTypes.LOAD_INCIDENCE_SUCCESS;
  constructor(public payload: IncidenceData) {}
}

export class LoadIncidenceFail implements Action {
  readonly type = IncidenceActionTypes.LOAD_INCIDENCE_FAIL;
  constructor(public payload: Error) {}
}

export type IncidenceAction =
  | LoadIncidence
  | LoadIncidenceSuccess
  | LoadIncidenceFail;
