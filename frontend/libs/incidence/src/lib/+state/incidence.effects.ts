import { IncidenceService } from '../incidence.service';
import { ActionsService } from '@plusOne/shared';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { DataPersistence } from '@nrwl/nx';
import { of } from 'rxjs';
import { catchError, concatMap, exhaustMap, map, mergeMap, withLatestFrom } from 'rxjs/operators';
import * as fromActions from './incidence.actions';

import {
  LoadIncidence,
  IncidenceActionTypes
} from './incidence.actions';
import { NgrxFormsFacade, ResetForm, SetErrors } from '@plusOne/ngrx-forms';

@Injectable()
export class IncidenceEffects {
  @Effect()
  loadIncidence = this.actions
    .ofType<LoadIncidence>(IncidenceActionTypes.LOAD_INCIDENCE)
    .pipe(
      concatMap(action =>
        this.incidenceService
          .get(action.payload)
          .pipe(
            map(results => new fromActions.LoadIncidenceSuccess(results)),
            catchError(error => of(new fromActions.LoadIncidenceFail(error)))
          )
      )
    );

  constructor(
    private actions: Actions,
    private incidenceService: IncidenceService,
    private actionsService: ActionsService,
    private ngrxFormsFacade: NgrxFormsFacade
  ) {}
}
