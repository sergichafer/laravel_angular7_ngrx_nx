import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IncidenceState, Incidence } from './incidence.reducer';

const getIncidence = createFeatureSelector<Incidence>('incidence');
export const getIncidenceData = createSelector(getIncidence, (state: Incidence) => state.data);
export const getIncidenceLoaded = createSelector(getIncidence, (state: Incidence) => state.loaded);

export const incidenceQuery = {
  getIncidenceData,
  getIncidenceLoaded,
};
