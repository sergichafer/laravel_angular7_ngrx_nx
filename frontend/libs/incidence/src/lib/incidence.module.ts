import { SharedModule } from '@plusOne/shared';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { IncidenceEffects } from './+state/incidence.effects';
import { IncidenceFacade } from './+state/incidence.facade';
import { incidenceInitialState, incidenceReducer } from './+state/incidence.reducer';
import { IncidenceGuardService } from './incidence-guard.service';
import { IncidenceComponent } from './incidence.component';
import { IncidenceService } from './incidence.service';
import { MarkdownPipe } from './markdown.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: IncidenceComponent, canActivate: [IncidenceGuardService]}]),
    StoreModule.forFeature('incidence', incidenceReducer, { initialState: incidenceInitialState }),
    EffectsModule.forFeature([IncidenceEffects]),
    SharedModule
  ],
  providers: [IncidenceEffects, IncidenceService, IncidenceGuardService, IncidenceFacade],
  declarations: [IncidenceComponent, MarkdownPipe]
})
export class IncidenceModule {}
