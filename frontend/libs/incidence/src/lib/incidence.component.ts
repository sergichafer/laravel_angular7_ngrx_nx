import { IncidenceData } from '@plusOne/api';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { IncidenceFacade } from './+state/incidence.facade';

@Component({
  selector: 'app-incidence',
  templateUrl: './incidence.component.html',
  styleUrls: ['./incidence.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IncidenceComponent implements OnInit {
  incidence$: Observable<IncidenceData>;

  constructor(
    private facade: IncidenceFacade,
  ) {}

  ngOnInit() {
    this.incidence$ = this.facade.incidence$;
  }
}
