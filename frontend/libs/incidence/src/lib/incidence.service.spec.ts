import { ApiService } from '@plusOne/api';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';

import { IncidenceService } from './incidence.service';

describe('IncidenceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [IncidenceService, ApiService]
    });
  });

  it(
    'should be created',
    inject([IncidenceService], (service: IncidenceService) => {
      expect(service).toBeTruthy();
    })
  );
});
