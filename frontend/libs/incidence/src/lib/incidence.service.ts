import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService, IncidenceData } from '@plusOne/api';
import { map } from 'rxjs/operators';

@Injectable()
export class IncidenceService {
  constructor(private apiService: ApiService) {}

  get(slug: string): Observable<IncidenceData> {
    return this.apiService.get('/incidences/' + slug).pipe(map((data: any) => data.incidence));
  }
}
