import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import * as fromIncidence from './+state/incidence.reducer';
import { filter, take, switchMap, tap } from 'rxjs/operators';
import * as fromActions from './+state/incidence.actions';
import { IncidenceFacade } from './+state/incidence.facade';

@Injectable()
export class IncidenceGuardService implements CanActivate {
  constructor(private facade: IncidenceFacade) {}

  waitForIncidenceToLoad(): Observable<boolean> {
    return this.facade.incidenceLoaded$.pipe(filter(loaded => loaded), take(1));
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const slug = route.params['slug'];
    this.facade.loadIncidence(slug);

    return this.waitForIncidenceToLoad();
  }
}
