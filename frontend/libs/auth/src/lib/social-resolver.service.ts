import { Injectable, } from '@angular/core';
import { User } from '@plusOne/api';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

import { map } from 'rxjs/operators';

@Injectable()
export class SocialResolver implements Resolve<User> {
  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  resolve(): Observable<any> {
    return this.authService.socialGet()
      .pipe(map(data => {
          //window.location.reload();
          return this.router.navigateByUrl('/');
        }));

  }
}