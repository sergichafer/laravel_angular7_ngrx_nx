import { ApiService, User } from '@plusOne/api';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorageJwtService } from './local-storage-jwt.service';

@Injectable()
export class AuthService {
	constructor(
		private apiService: ApiService,
		private localStorageJwtService: LocalStorageJwtService	
	) { }

	user(): Observable<{ user: User }> {
		return this.apiService.get('/user');
	}

	login(credentials: { email: string, password: string }): Observable<User> {
		return this.apiService.post('/users/login', { user: credentials }).pipe(map(r => r.user));
	}

	register(credentials: { username: string, email: string, password: string }): Observable<User> {
		return this.apiService.post('/users', { user: credentials }).pipe(map(r => r.user));
	}

	socialGet(): Observable<User> {
		return this.apiService.get('/users/socialGet')
		  .pipe(map(
		  data => {
				this.localStorageJwtService.setItem(data.user.token);
				return data;
		  }
		));
	  }
}
