import { ApiService } from '@plusOne/api';
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { IncidenceListConfig } from './+state/incidence-list.reducer';

@Injectable()
export class IncidenceListService {
  constructor(private apiService: ApiService) {}

  query(config: IncidenceListConfig): Observable<any> {
/*     return this.apiService.get(
      '/incidences' + (config.type === 'FEED' ? '/feed' : ''),
      this.toHttpParams(config.filters)
    ); */
    return this.apiService.get('/incidences',
      this.toHttpParams(config.filters)
    );
  }

  private toHttpParams(params) {
    return Object.getOwnPropertyNames(params).reduce((p, key) => p.set(key, params[key]), new HttpParams());
  }
}
