import { ApiService } from '@plusOne/api';
import { inject, TestBed } from '@angular/core/testing';

import { IncidenceListService } from './incidence-list.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('IncidenceListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [IncidenceListService, ApiService]
    });
  });

  it(
    'should be created',
    inject([IncidenceListService], (service: IncidenceListService) => {
      expect(service).toBeTruthy();
    })
  );
});
