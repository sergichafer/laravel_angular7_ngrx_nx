import { IncidenceData } from '@plusOne/api';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { withLatestFrom } from 'rxjs/operators';

import { IncidenceListFacade } from './+state/incidence-list.facade';
import { IncidenceListConfig } from './+state/incidence-list.reducer';

@Component({
  selector: 'app-incidence-list',
  templateUrl: './incidence-list.component.html',
  styleUrls: ['./incidence-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IncidenceListComponent implements OnInit {
  totalPages$: Observable<number[]>;
  incidences$: Observable<IncidenceData[]>;
  listConfig$: Observable<IncidenceListConfig>;
  isLoading$: Observable<boolean>;

  constructor(private facade: IncidenceListFacade) {}

  ngOnInit() {
    this.totalPages$ = this.facade.totalPages$;
    this.incidences$ = this.facade.incidences$;
    this.listConfig$ = this.facade.listConfig$;
    this.isLoading$ = this.facade.isLoading$;
  }

  navigateToIncidence(slug: string) {
    this.facade.navigateToIncidence(slug);
  }

  setPage(page: number) {
    this.facade.setPage(page);
  }
}
