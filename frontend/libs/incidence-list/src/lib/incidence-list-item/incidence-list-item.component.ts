import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { IncidenceData } from '@plusOne/api';

@Component({
  selector: 'app-incidence-list-item',
  templateUrl: './incidence-list-item.component.html',
  styleUrls: ['./incidence-list-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IncidenceListItemComponent {
  @Input() incidence: IncidenceData;
  @Output() navigateToIncidence: EventEmitter<string> = new EventEmitter();
}
