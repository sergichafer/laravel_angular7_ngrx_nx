import { ActionsService } from '@plusOne/shared';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, concatMap, map, withLatestFrom } from 'rxjs/operators';

import { IncidenceListService } from '../incidence-list.service';
import { IncidenceListActionTypes, LoadIncidences, SetListConfig, SetListPage } from './incidence-list.actions';
import * as fromActions from './incidence-list.actions';
import { IncidenceListFacade } from './incidence-list.facade';

@Injectable()
export class IncidenceListEffects {
  @Effect()
  setListPage = this.actions
    .ofType<SetListPage>(IncidenceListActionTypes.SET_LIST_PAGE)
    .pipe(map(() => ({ type: IncidenceListActionTypes.LOAD_INCIDENCES })));

  @Effect()
  setListTag = this.actions
    .ofType<SetListConfig>(IncidenceListActionTypes.SET_LIST_CONFIG)
    .pipe(map(() => new fromActions.LoadIncidences()));

  @Effect()
  loadIncidences = this.actions.ofType<LoadIncidences>(IncidenceListActionTypes.LOAD_INCIDENCES).pipe(
    withLatestFrom(this.facade.listConfig$),
    concatMap(([_, config]) =>
      this.incidenceListService.query(config).pipe(
        map(
          results =>
            new fromActions.LoadIncidencesSuccess({
              incidences: results.incidences,
              incidencesCount: results.incidencesCount
            })
        ),
        catchError(error => of(new fromActions.LoadIncidencesFail(error)))
      )
    )
  );

  constructor(
    private actions: Actions,
    private incidenceListService: IncidenceListService,
    private actionsService: ActionsService,
    private facade: IncidenceListFacade
  ) {}
}
