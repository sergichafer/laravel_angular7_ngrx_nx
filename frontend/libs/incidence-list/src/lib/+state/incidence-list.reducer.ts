import { IncidenceData } from '@plusOne/api';

import { IncidenceListAction, IncidenceListActionTypes } from './incidence-list.actions';

export interface IncidenceList {
  listConfig: IncidenceListConfig;
  incidences: Incidences;
}

export interface IncidenceListState {
  readonly incidenceList: IncidenceList;
}

export interface IncidenceListConfig {
  type: ListType;
  currentPage: number;
  filters: Filters;
}

export interface Filters {
  tag?: string;
  limit?: number;
  offset?: number;
}

export type ListType = 'ALL' | 'FEED';

export interface Incidences {
  entities: IncidenceData[];
  incidencesCount: number;
  loaded: boolean;
  loading: boolean;
}

export const incidenceListInitialState: IncidenceList = {
  listConfig: {
    type: 'ALL',
    currentPage: 1,
    filters: {
      limit: 3
    }
  },
  incidences: {
    entities: [],
    incidencesCount: 0,
    loaded: false,
    loading: false
  }
};

export function incidenceListReducer(state: IncidenceList, action: IncidenceListAction): IncidenceList {
  switch (action.type) {
    case IncidenceListActionTypes.SET_LIST_PAGE: {
      const filters = { ...state.listConfig.filters, offset: state.listConfig.filters.limit * (action.payload - 1) };
      const listConfig = { ...state.listConfig, currentPage: action.payload, filters };
      return { ...state, listConfig };
    }
    case IncidenceListActionTypes.SET_LIST_CONFIG: {
      return { ...state, listConfig: action.payload };
    }
    case IncidenceListActionTypes.LOAD_INCIDENCES: {
      const incidences = { ...state.incidences, loading: true };
      return { ...state, incidences };
    }
    case IncidenceListActionTypes.LOAD_INCIDENCES_SUCCESS: {
      const incidences = {
        ...state.incidences,
        entities: action.payload.incidences,
        incidencesCount: action.payload.incidencesCount,
        loading: false,
        loaded: true
      };
      return { ...state, incidences };
    }
    case IncidenceListActionTypes.LOAD_INCIDENCES_FAIL: {
      const incidences = { ...state.incidences, entities: [], incidencesCount: 0, loading: false, loaded: true };
      return { ...state, incidences };
    }
    default: {
      return state;
    }
  }
}

function replaceIncidence(incidences: Incidences, payload: IncidenceData): Incidences {
  const incidenceIndex = incidences.entities.findIndex(a => a.slug === payload.slug);
  const entities = [
    ...incidences.entities.slice(0, incidenceIndex),
    Object.assign({}, incidences.entities[incidenceIndex], payload),
    ...incidences.entities.slice(incidenceIndex + 1)
  ];
  return { ...incidences, entities, loading: false, loaded: true };
}
