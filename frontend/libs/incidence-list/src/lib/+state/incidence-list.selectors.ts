import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IncidenceListState, IncidenceList } from './incidence-list.reducer';

const getIncidenceList = createFeatureSelector<IncidenceList>('incidenceList');
export const getListConfig = createSelector(getIncidenceList, (state: IncidenceList) => state.listConfig);
export const getIncidences = createSelector(getIncidenceList, (state: IncidenceList) => state.incidences.entities);
export const getIncidencesCount = createSelector(getIncidenceList, (state: IncidenceList) => state.incidences.incidencesCount);
export const isLoading = createSelector(getIncidenceList, (state: IncidenceList) => state.incidences.loading);

export const incidenceListQuery = {
  getListConfig,
  getIncidences,
  getIncidencesCount,
  isLoading
};
