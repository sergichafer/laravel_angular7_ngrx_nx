import { ApiService } from '@plusOne/api';
import { ActionsService } from '@plusOne/shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { StoreModule } from '@ngrx/store';
import { DataPersistence } from '@nrwl/nx';
import { hot } from '@nrwl/nx/testing';

import { IncidenceListService } from '../incidence-list.service';
import { IncidenceListEffects } from './incidence-list.effects';
import { IncidenceListFacade } from './incidence-list.facade';

describe('IncidenceListEffects', () => {
	let actions;
	let effects: IncidenceListEffects;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [StoreModule.forRoot({}), HttpClientTestingModule],
			providers: [
				IncidenceListEffects,
				DataPersistence,
				provideMockActions(() => actions),
				ActionsService,
				IncidenceListService,
				ApiService,
				IncidenceListFacade
			]
		});

		effects = TestBed.get(IncidenceListEffects);
	});

	describe('someEffect', () => {
		it('should work', async () => {
			actions = hot('-a-|', { a: { type: 'LOAD_DATA' } });
			expect(true).toBeTruthy();
		});
	});
});
