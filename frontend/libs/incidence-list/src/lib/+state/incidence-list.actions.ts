import { IncidenceData } from '@plusOne/api';
import { Action } from '@ngrx/store';

import { IncidenceListConfig } from './incidence-list.reducer';

export enum IncidenceListActionTypes {
  SET_LIST_PAGE = '[incidence-list] SET_LIST_PAGE',
  SET_LIST_CONFIG = '[incidence-list] SET_LIST_CONFIG',
  LOAD_INCIDENCES = '[incidence-list] LOAD_INCIDENCES',
  LOAD_INCIDENCES_SUCCESS = '[incidence-list] LOAD_INCIDENCES_SUCCESS',
  LOAD_INCIDENCES_FAIL = '[incidence-list] LOAD_INCIDENCES_FAIL',
}

export class SetListPage implements Action {
  readonly type = '[incidence-list] SET_LIST_PAGE';
  constructor(public payload: number) {}
}

export class SetListConfig implements Action {
  readonly type = '[incidence-list] SET_LIST_CONFIG';
  constructor(public payload: IncidenceListConfig) {}
}

export class LoadIncidences implements Action {
  readonly type = '[incidence-list] LOAD_INCIDENCES';
  constructor() {}
}

export class LoadIncidencesSuccess implements Action {
  readonly type = '[incidence-list] LOAD_INCIDENCES_SUCCESS';
  constructor(public payload: { incidences: IncidenceData[]; incidencesCount: number }) {}
}

export class LoadIncidencesFail implements Action {
  readonly type = '[incidence-list] LOAD_INCIDENCES_FAIL';
  constructor(public payload: Error) {}
}

export type IncidenceListAction =
  | SetListPage
  | SetListConfig
  | LoadIncidences
  | LoadIncidencesSuccess
  | LoadIncidencesFail;
