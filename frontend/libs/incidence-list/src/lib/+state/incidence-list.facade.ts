import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { IncidenceListState, IncidenceListConfig } from './incidence-list.reducer';
import { incidenceListQuery } from './incidence-list.selectors';
import { SetListPage, SetListConfig } from './incidence-list.actions';
import { withLatestFrom, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class IncidenceListFacade {
  incidences$ = this.store.select(incidenceListQuery.getIncidences);
  listConfig$ = this.store.select(incidenceListQuery.getListConfig);
  isLoading$ = this.store.select(incidenceListQuery.isLoading);
  incidencesCount$ = this.store.select(incidenceListQuery.getIncidencesCount);
  totalPages$ = this.incidencesCount$.pipe(
    withLatestFrom(this.listConfig$),
    map(([incidencesCount, config]) => {
      return Array.from(new Array(Math.ceil(incidencesCount / config.filters.limit)), (val, index) => index + 1);
    })
  );

  constructor(private store: Store<IncidenceListState>) {}

  navigateToIncidence(slug: string) {
    this.store.dispatch({ type: '[router] Go', payload: { path: ['/incidence', slug] } });
  }

  setPage(page: number) {
    this.store.dispatch(new SetListPage(page));
  }

  setListConfig(config: IncidenceListConfig) {
    this.store.dispatch(new SetListConfig(config));
  }
}
