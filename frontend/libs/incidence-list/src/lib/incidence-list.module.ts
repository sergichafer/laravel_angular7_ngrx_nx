import { SharedModule } from '@plusOne/shared';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { IncidenceListEffects } from './+state/incidence-list.effects';
import { IncidenceListFacade } from './+state/incidence-list.facade';
import { incidenceListInitialState, incidenceListReducer } from './+state/incidence-list.reducer';
import { IncidenceListItemComponent } from './incidence-list-item/incidence-list-item.component';
import { IncidenceListComponent } from './incidence-list.component';
import { IncidenceListService } from './incidence-list.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    StoreModule.forFeature('incidenceList', incidenceListReducer, { initialState: incidenceListInitialState }),
    EffectsModule.forFeature([IncidenceListEffects])
  ],
  declarations: [IncidenceListComponent, IncidenceListItemComponent],
  providers: [IncidenceListService, IncidenceListEffects, IncidenceListFacade],
  exports: [IncidenceListComponent]
})
export class IncidenceListModule {}
