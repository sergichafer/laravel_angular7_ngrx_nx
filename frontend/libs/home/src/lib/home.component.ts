import { Observable, Subject } from 'rxjs';
import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';

import * as fromHome from './+state/home.reducer';
import * as fromAuth from '@plusOne/auth';
import { withLatestFrom, tap, takeUntil } from 'rxjs/operators';
import * as fromIncidenceList from '@plusOne/incidence-list';
import { IncidenceListConfig } from '@plusOne/incidence-list';
import { incidenceListInitialState, IncidenceListFacade } from '@plusOne/incidence-list';
import { AuthFacade } from '@plusOne/auth';
import { HomeFacade } from './+state/home.facade';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit, OnDestroy {
  listConfig$: Observable<IncidenceListConfig>;
  tags$: Observable<string[]>;
  isAuthenticated: boolean;
  unsubscribe$: Subject<void> = new Subject();

  constructor(
    private facade: HomeFacade,
    private router: Router,
    private incidenceListFacade: IncidenceListFacade,
    private authFacade: AuthFacade
  ) {}

  ngOnInit() {
    this.authFacade.isLoggedIn$.pipe(takeUntil(this.unsubscribe$)).subscribe(isLoggedIn => {
      this.isAuthenticated = isLoggedIn;
      this.getIncidences();
    });
    this.listConfig$ = this.incidenceListFacade.listConfig$;
    this.tags$ = this.facade.tags$;
  }

  setListTo(type: string = 'ALL') {
    this.incidenceListFacade.setListConfig(<IncidenceListConfig>{
      ...incidenceListInitialState.listConfig,
      type
    });
  }

  getIncidences() {
    this.setListTo('ALL');
  }

  setListTag(tag: string) {
    this.incidenceListFacade.setListConfig(<IncidenceListConfig>{
      ...incidenceListInitialState.listConfig,
      filters: {
        ...incidenceListInitialState.listConfig.filters,
        tag
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
