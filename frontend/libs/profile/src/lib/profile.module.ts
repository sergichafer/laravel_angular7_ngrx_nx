import { AuthGuardService } from '@plusOne/auth';
import { SharedModule } from '@plusOne/shared';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ProfileEffects } from './+state/profile.effects';
import { ProfileFacade } from './+state/profile.facade';
import { profileInitialState, profileReducer } from './+state/profile.reducer';
import {
  ProfileResolverService
} from './profile-resolver.service';
import { ProfileComponent } from './profile.component';
import { ProfileService } from './profile.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProfileComponent,
        resolve: { ProfileResolverService },
        canActivate: [AuthGuardService],
      }
    ]),
    StoreModule.forFeature('profile', profileReducer, { initialState: profileInitialState }),
    EffectsModule.forFeature([ProfileEffects])
  ],
  providers: [
    ProfileEffects,
    ProfileService,
    ProfileResolverService,
    ProfileFacade
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule {}
