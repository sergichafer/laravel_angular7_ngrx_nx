import { Profile } from './profile.reducer';
import { Action } from '@ngrx/store';

export enum ProfileActionTypes {
  GET_PROFILE = '[profile] GET_PROFILE',
  GET_PROFILE_SUCCESS = '[profile] GET_PROFILE_SUCCESS',
  GET_PROFILE_FAIL = '[profile] GET_PROFILE_FAIL'
}

export class GetProfile implements Action {
  readonly type = ProfileActionTypes.GET_PROFILE;
  constructor(public payload: string) {}
}

export class GetProfileSuccess implements Action {
  readonly type = ProfileActionTypes.GET_PROFILE_SUCCESS;
  constructor(public payload: Profile) {}
}

export class GetProfileFail implements Action {
  readonly type = ProfileActionTypes.GET_PROFILE_FAIL;
  constructor(public payload: Error) {}
}

export type ProfileAction =
  | GetProfile
  | GetProfileSuccess
  | GetProfileFail;
