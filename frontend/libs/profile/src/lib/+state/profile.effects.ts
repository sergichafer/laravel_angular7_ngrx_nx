import { ActionsService } from '@plusOne/shared';
import { GetProfile, ProfileActionTypes } from '../+state/profile.actions';
import { ProfileService } from '../profile.service';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, concatMap, groupBy, map, mergeMap, switchMap } from 'rxjs/operators';

@Injectable()
export class ProfileEffects {
  @Effect()
  getProfile = this.actions.ofType<GetProfile>(ProfileActionTypes.GET_PROFILE).pipe(
    groupBy(action => action.payload),
    mergeMap(group =>
      group.pipe(
        map(action => action.payload),
        switchMap(username =>
          this.profileService.getProfile(username).pipe(
            map(results => ({
              type: ProfileActionTypes.GET_PROFILE_SUCCESS,
              payload: results
            })),
            catchError(error =>
              of({
                type: ProfileActionTypes.GET_PROFILE_FAIL,
                payload: error
              })
            )
          )
        )
      )
    )
  );

  constructor(
    private actions: Actions,
    private actionsService: ActionsService,
    private profileService: ProfileService
  ) {}
}
