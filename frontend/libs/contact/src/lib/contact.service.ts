import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService, ContactData } from '@plusOne/api';
import { map } from 'rxjs/operators';

@Injectable()
export class ContactService {
  constructor(private apiService: ApiService) {}
  save(contactData): Observable<ContactData> {
    return this.apiService.post('/email', {contactData : contactData})
    .pipe(map(data => data.contactData)); 
  }
}
