import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';
import { ContactService } from './contact.service';
import { ToastrModule } from 'ng6-toastr-notifications';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    FormsModule, 
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: ContactComponent
      }    
    ]),
    ToastrModule.forRoot()
  ],
  declarations: [ContactComponent],
  providers: [ContactService]
})
export class ContactModule {}
