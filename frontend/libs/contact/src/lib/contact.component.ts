import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { ContactService } from './contact.service';
import { ContactData } from '@plusOne/api';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  isSubmitting = false;
  contactData: ContactData = {} as ContactData;
  contactForm: FormGroup;

  constructor(
      private router: Router,
      private contactService: ContactService,
      private fb: FormBuilder,
      public toastr: ToastrManager
  ) {
    // use the FormBuilder to create a form group
    this.contactForm = this.fb.group({
      title: ['', Validators.required],
      mail: ['', [Validators.required, Validators.email]],
      body: ['', Validators.required]
    });

  }


  submitForm() {
    // update the model
    console.log(this.contactForm.value);
    this.updateContactModel(this.contactForm.value);

    // post the changes
    this.contactService.save(this.contactData).subscribe(
        contactData => {
          this.toastr.successToastr('Your mail has been sent.', 'Success!');
          this.contactForm = this.fb.group({
            title: '',
            mail: '',
            body: ''
          });
        },
        err => {
          this.isSubmitting = false;
          this.toastr.errorToastr('There was a problem sending your mail.', 'Oops!');
        },
    );
  }
  updateContactModel(values: Object) {
    Object.assign(this.contactData, values);
  }
}
