import { ApiService } from '@plusOne/api';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';

import { ContactService } from './contact.service';

describe('ContactService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ContactService, ApiService]
    });
  });

  it(
    'should be created',
    inject([ContactService], (service: ContactService) => {
      expect(service).toBeTruthy();
    })
  );
});
