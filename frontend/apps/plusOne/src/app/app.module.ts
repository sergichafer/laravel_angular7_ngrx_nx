import { ApiModule } from '@plusOne/api';
import { AuthModule } from '@plusOne/auth';
import { NgrxErrorModule } from '@plusOne/ngrx-error';
import { NgrxRouterModule } from '@plusOne/ngrx-router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NxModule } from '@nrwl/nx';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { FooterComponent } from './layout/footer/footer.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    ApiModule,
    AuthModule,
    BrowserModule,
    BrowserAnimationsModule,
    NxModule.forRoot(),
    RouterModule.forRoot(
      [
        { path: '', loadChildren: '@plusOne/home#HomeModule' },
        { path: 'incidence/:slug', loadChildren: '@plusOne/incidence#IncidenceModule' },
        { path: 'settings', loadChildren: '@plusOne/settings#SettingsModule' },
        { path: 'contact', loadChildren: '@plusOne/contact#ContactModule' },
        { path: 'profile/:username', loadChildren: '@plusOne/profile#ProfileModule' }
      ],
      {
        initialNavigation: 'enabled',
        useHash: true
      }
    ),
    StoreModule.forRoot({}, { metaReducers: !environment.production ? [storeFreeze] : [] }),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    NgrxRouterModule,
    NgrxErrorModule
  ],
  declarations: [AppComponent, FooterComponent, NavbarComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
