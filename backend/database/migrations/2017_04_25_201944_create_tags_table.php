<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

            $table->unique('name');
        });

        Schema::create('incidence_tag', function (Blueprint $table) {
            $table->unsignedInteger('incidence_id');
            $table->unsignedInteger('tag_id');

            $table->primary(['incidence_id', 'tag_id']);

            $table->foreign('incidence_id')
                ->references('id')
                ->on('incidences')
                ->onDelete('cascade');

            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidence_tag');
        Schema::dropIfExists('tags');
    }
}
