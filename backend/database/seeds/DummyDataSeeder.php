<?php

use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{
    /**
     * Total number of users.
     *
     * @var int
     */
    protected $totalUsers = 10;

    /**
     * Total number of tags.
     *
     * @var int
     */
    protected $totalTags = 10;

    /**
     * Percentage of users with incidences.
     *
     * @var float Value should be between 0 - 1.0
     */
    protected $userWithIncidenceRatio = 0.8;

    /**
     * Maximum incidences that can be created by a user.
     *
     * @var int
     */
    protected $maxIncidencesByUser = 2;

    /**
     * Maximum tags that can be attached to an incidence.
     *
     * @var int
     */
    protected $maxIncidenceTags = 3;


    /**
     * Populate the database with dummy data for testing.
     * Complete dummy data generation including relationships.
     * Set the property values as required before running database seeder.
     *
     * @param \Faker\Generator $faker
     */
    public function run(\Faker\Generator $faker)
    {
        $users = factory(\App\User::class)->times($this->totalUsers)->create();

        $tags = factory(\App\Tag::class)->times($this->totalTags)->create();

        $users->random((int) $this->totalUsers * $this->userWithIncidenceRatio)
            ->each(function ($user) use ($faker, $tags) {
                $user->incidences()
                    ->saveMany(
                        factory(\App\Incidence::class)
                        ->times($faker->numberBetween(1, $this->maxIncidencesByUser))
                        ->make()
                    )
                    ->each(function ($incidence) use ($faker, $tags) {
                        $incidence->tags()->attach(
                            $tags->random($faker->numberBetween(1, min($this->maxIncidenceTags, $this->totalTags)))
                        );
                    });
            });
    }
}
