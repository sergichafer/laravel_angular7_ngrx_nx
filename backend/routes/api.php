<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    
    Route::post('users/login', 'AuthController@login');
    Route::post('users', 'AuthController@register');

    Route::get('user', 'UserController@index');
    
    Route::get('users/login/{provider}', 'AuthController@socialAuth')->where(['provider' => 'google']);
    Route::get('users/socialGet', 'AuthController@socialGet');
    Route::get('users/login/{provider}/callback', 'AuthController@social')->where(['provider' => 'google']);

    Route::match(['put', 'patch'], 'user', 'UserController@update');

    Route::get('profiles/{user}', 'ProfileController@show');

    Route::get('incidences/feed', 'FeedController@index');

    Route::resource('incidences', 'IncidenceController', [
        'except' => [
            'create', 'edit'
        ]
    ]);
    //Route::get('incidences/{incidence}', 'IncidenceController@show');

    Route::get('tags', 'TagController@index');
    Route::post('email', 'EmailController@email');
});