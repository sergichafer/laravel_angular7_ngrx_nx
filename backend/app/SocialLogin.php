<?php
namespace App;

use Socialite;

class SocialLogin 
{
    public function authenticate($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect('http://127.0.0.1:8000/api/users/login/google/callback');
    }

    public function login($provider)
    {
        $socialUserInfo = Socialite::driver($provider)->stateless()->user();
        return $socialUserInfo;
        
        /*
        try {
            $socialUserInfo = Socialite::driver($provider)->user();
            $user = User::firstOrCreate(['email' => $socialUserInfo->getEmail()]);
            $socialProfile = $user->socialProfile ?: new SocialLoginProfile;
            $providerField = "{$provider}_id";
            $socialProfile->{$providerField} = $socialUserInfo->getId();
            $user->socialProfile()->save($socialProfile);
            auth()->login($user);
        } catch (Exception $e) {
            throw new SocialAuthException("failed to authenticate with $provider");
        }
        */
    }
}