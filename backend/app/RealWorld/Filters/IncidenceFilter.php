<?php

namespace App\RealWorld\Filters;

use App\Tag;
use App\User;

class IncidenceFilter extends Filter
{
    /**
     * Filter by author username.
     * Get all the incidences by the user with given username.
     *
     * @param $username
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function author($username)
    {
        $user = User::whereUsername($username)->first();

        $userId = $user ? $user->id : null;

        return $this->builder->whereUserId($userId);
    }

    /**
     * Filter by tag name.
     * Get all the incidences tagged by the given tag name.
     *
     * @param $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function tag($name)
    {
        $tag = Tag::whereName($name)->first();

        $incidenceIds = $tag ? $tag->incidences()->pluck('incidence_id')->toArray() : [];

        return $this->builder->whereIn('id', $incidenceIds);
    }
}