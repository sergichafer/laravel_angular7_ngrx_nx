<?php

namespace App\RealWorld\Transformers;

class IncidenceTransformer extends Transformer
{
    protected $resourceName = 'incidence';

    public function transform($data)
    {
        return [
            'slug'              => $data['slug'],
            'title'             => $data['title'],
            'description'       => $data['description'],
            'image'             => $data['image'],
            'body'              => $data['body'],
            'tagList'           => $data['tagList'],
            'createdAt'         => $data['created_at']->toAtomString(),
            'updatedAt'         => $data['updated_at']->toAtomString(),
        ];
    }
}