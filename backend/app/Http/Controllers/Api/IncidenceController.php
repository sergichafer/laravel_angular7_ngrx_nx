<?php

namespace App\Http\Controllers\Api;

use App\Tag;
use App\Incidence;
use App\RealWorld\Paginate\Paginate;
use App\RealWorld\Filters\IncidenceFilter;
use App\Http\Requests\Api\CreateIncidence;
use App\Http\Requests\Api\UpdateIncidence;
use App\Http\Requests\Api\DeleteIncidence;
use App\RealWorld\Transformers\IncidenceTransformer;

class IncidenceController extends ApiController
{
    /**
     * IncidenceController constructor.
     *
     * @param IncidenceTransformer $transformer
     */
    public function __construct(IncidenceTransformer $transformer)
    {
        $this->transformer = $transformer;

        $this->middleware('auth.api')->except(['index', 'show']);
        $this->middleware('auth.api:optional')->only(['index', 'show']);
    }

    /**
     * Get all the incidence.
     *
     * @param IncidenceFilter $filter
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IncidenceFilter $filter)
    {
        $incidences = new Paginate(Incidence::loadRelations()->filter($filter));

        return $this->respondWithPagination($incidences);
    }

    /**
     * Create a new incidence and return the incidence if successful.
     *
     * @param CreateIncidence $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateIncidence $request)
    {
        $user = auth()->user();

        $incidence = $user->incidences()->create([
            'title' => $request->input('incidence.title'),
            'description' => $request->input('incidence.description'),
            'image' => $request->input('incidence.image'),
            'body' => $request->input('incidence.body'),
        ]);

        $inputTags = $request->input('incidence.tagList');

        if ($inputTags && ! empty($inputTags)) {

            $tags = array_map(function($name) {
                return Tag::firstOrCreate(['name' => $name])->id;
            }, $inputTags);

            $incidence->tags()->attach($tags);
        }

        return $this->respondWithTransformer($incidence);
    }

    /**
     * Get the incidences given by its slug.
     *
     * @param Incidence $incidence
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Incidence $incidence)
    {
        return $this->respondWithTransformer($incidence);
    }

    /**
     * Update the incidence given by its slug and return the incidence if successful.
     *
     * @param UpdateIncidence $request
     * @param Incidence $incidence
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateIncidence $request, Incidence $incidence)
    {
        if ($request->has('incidence')) {
            $incidence->update($request->get('incidence'));
        }

        return $this->respondWithTransformer($incidence);
    }

    /**
     * Delete the incidence given by its slug.
     *
     * @param DeleteIncidence $request
     * @param Incidence $incidence
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteIncidence $request, Incidence $incidence)
    {
        $incidence->delete();

        return $this->respondSuccess();
    }
}
