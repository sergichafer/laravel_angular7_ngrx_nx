<?php 

namespace App\Http\Controllers\Api;
use App\Http\Requests\Api\Contact;
use Mail;

class EmailController extends ApiController { 	
    public function email( Contact $request ){
        $data = array(
            'title' => $request->input('contactData.title'),
            'email' => $request->input('contactData.mail'),
            'body' => $request->input('contactData.body')
        );
        
        $sent = Mail::send('emails.email', $data, function ($message){
            $message->subject('Test email');
            $message->from('ruralshoponti@gmail.com', 'ruralshoponti');
            $message->to('sergislm4@gmail.com');
        });
        if($sent) dd("something wrong"); //var_dump + exit
        
        return response()->json(['contactData' => 'Request completed']);
        //echo json_encode("Your email has been sent successfully");
        //return "Your email has been sent successfully";
    }
}