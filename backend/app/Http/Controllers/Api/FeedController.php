<?php

namespace App\Http\Controllers\Api;

use App\RealWorld\Paginate\Paginate;
use App\RealWorld\Transformers\IncidenceTransformer;

class FeedController extends ApiController
{
    /**
     * FeedController constructor.
     *
     * @param IncidenceTransformer $transformer
     */
    public function __construct(IncidenceTransformer $transformer)
    {
        $this->transformer = $transformer;

        $this->middleware('auth.api');
    }

    /**
     * Get all the incidences of users that are followed by the authenticated user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = auth()->user();

        $incidences = new Paginate($user->feed());

        return $this->respondWithPagination($incidences);
    }
}
