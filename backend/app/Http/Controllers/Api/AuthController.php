<?php

namespace App\Http\Controllers\Api;

use Auth;
use Socialite;
use App\User;
use App\SocialLogin;
use App\Http\Requests\Api\LoginUser;
use App\Http\Requests\Api\RegisterUser;
use App\RealWorld\Transformers\UserTransformer;
use Illuminate\Support\Facades\Storage;

class AuthController extends ApiController
{
    protected $SocialLogin;
    /**
     * AuthController constructor.
     *
     * @param UserTransformer $transformer
     */
    public function __construct(UserTransformer $transformer, SocialLogin $SocialLogin)
    {
        $this->transformer = $transformer;
        $this->SocialLogin = $SocialLogin;
    }

    /**
     * Login user and return the user if successful.
     *
     * @param LoginUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginUser $request)
    {
        $credentials = $request->only('user.email', 'user.password');
        $credentials = $credentials['user'];

        if (! Auth::once($credentials)) {
            return $this->respondFailedLogin();
        }

        return $this->respondWithTransformer(auth()->user());
    }

    /**
     * Register a new user and return the user if successful.
     *
     * @param RegisterUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterUser $request)
    {
        $user = User::create([
            'username' => $request->input('user.username'),
            'email' => $request->input('user.email'),
            'password' => $request->input('user.password'),
        ]);

        return $this->respondWithTransformer($user);
    }

    public function socialAuth($provider)
    {
        return $this->SocialLogin->authenticate($provider);
    }
 
    public function social($provider)
    {   
        $socialized = $this->SocialLogin->login($provider);
        $credentials = [
            'email'    => $socialized->email,
            'password' => $socialized->id,
        ];
        if (! Auth::once($credentials)) {
            $registered = User::create([
                'username' => $socialized->name,
                'email' => $socialized->email,
                'password' => $socialized->id,
            ]);
        }
        $user = User::where('email', $socialized->email) -> first();

        Storage::disk('local')->put('file.txt', $this->respondWithTransformer($user));
        
        return redirect('http://localhost:4200/#/sociallogin');

    }

    public function socialGet()
    {     
        $user = Storage::disk('local')->get('file.txt');
        $explode_id = explode("GMT", $user);
        Storage::disk('local')->delete('file.txt');
        return $explode_id[1];

    }
}
