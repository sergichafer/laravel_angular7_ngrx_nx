<?php

namespace App\Http\Requests\Api;

class CreateIncidence extends ApiRequest
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return $this->get('incidence') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'image' => 'required|string',
            'body' => 'required|string',
            'tagList' => 'sometimes|array',
        ];
    }
}