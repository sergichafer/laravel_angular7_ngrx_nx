<?php

namespace App\Http\Requests\Api;

class DeleteIncidence extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $incidence = $this->route('incidence');

        return $incidence->user_id == auth()->id();
    }
}
